# Getting the bot up and running

1.  You will need Node installed. **Ensure it is above v10.12.0**. Node will help run our bot. [Node Download Link ](https://nodejs.org/en/)
    
    *  Once Node is installed go to CMD or Terminal:

        **WINDOWS:** Press WIN + R and type `cmd` and press ENTER

        **MAC:** Press CMD + SPACE and type `terminal` and press ENTER

        **LINUX (Depends on your distribution):** Press CTRL + ALT + T

    
    *  Type: `node -v`, Press ENTER, then `npm -v` Press ENTER

    *  If the CMD/Terminal says `v10.12.0`, `6.4.1` or something similiar, you have successfully installed Node!

2.  Making the bot login to your account

    *   The details of the bot are stored in a file called `config.json` which is located in the `temp` folder. You will need to edit it so you can choose how the bot will run.
    
    *   For Windows and Mac users, there is a program called `EditConfig` which lets you edit this `config.json`file

    *   Here are the details you need to enter:

        `username`: the username of the bot when used to login to steam
        
        `password`: the password of the bot when used to login to steam
        
        `shared_secret`: used to instantly generate the steam mobile guard code for your account, use this [link](https://www.youtube.com/watch?v=JjdOJVSZ9Mo) to get them, if you have problems contact me **NOTE:** Leaving this blank, means you will have to enter the steam guard code manually everytime you start the bot. 
        
        `identity_secret`: used to instantly accept confirmations for your account, use this [link](https://www.youtube.com/watch?v=JjdOJVSZ9Mo) to get them, if you have problems contact me **NOTE:** Leaving this blank, means you will have to confirm every trade manually (destroying the purpose of a bot). 
        
        `apiKey`: provided to you on purchase of the bot, normally it is already filled in
        
        `ownerID`: contains owners steamID64, use this [link](https://www.youtube.com/watch?v=hbPXa_FIq7Y) to get it, if running on your own account leave blank or as it is **NOTE:** Each bot can have upto 3 owners
        
        `acceptDonations`: would you like to accept donations? yes: set to true, no: set to false
        
        `acceptTradeHolds`: would you like to accept trade hold trades? yes: set to true, no: set to false
        
        `acceptFriendRequests`: would you like to accept friend requests? yes: set to true, no: set to false **NOTE:** Bot accepts friend requests from owner even if set to false. 
        
        `acceptGroupRequests`: would you like to accept group requests? yes: set to true, no: set to false  **NOTE:** Bot accepts group requests from owner even if set to false. 
        
        `meta`: do **NOT** touch this or edit it your bot will not work without it and will display an authentication error **NOTE:** This does not show up in `EditConfig` Program. 
    
    * How to edit the config: 
    
        **WINDOWS/MAC:** Open the file named `EditConfig`, enter the details of the bot and press "Submit".
    
        **LINUX:** Open the file `config.json` in the `temp` folder. Change the details in the quotation marks

    *   Once the config has been changed you are nearly done!

3.  Adjust prices of items
